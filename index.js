const http = require('http');
const PORT = 4000;

http.createServer(function (request, response){
	// a
	if (request.url === "/" && request.method === "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to Booking System')
	} 
	// b
	else if (request.url === "/profile" && request.method === "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to your profile')
	}
	// c
	else if (request.url === "/courses" && request.method === "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Here's our courses available")
	}
	// d
	else if (request.url === "/addCourse" && request.method === "POST"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Add course to our resources")
	}
	// e
	else if (request.url === "/updateCourse" && request.method === "PUT"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Update a course to our resources")
	}
	// f
	else if (request.url === "/archiveCourse" && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Archive courses to our resources")
	}

}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)